#!/bin/bash
echo "Installing Everything to Be Joko Project"
apt install pciutils
chmod +x *.sh
mv setup.json _setup.json.orig
cd ..
FILE=setup.json
if [ -f "$FILE" ]; then
    mv setup.json jokoproject/setup.json
else 
    mv jokoproject/_setup.json.orig jokoproject/setup.json 
fi
mv jokoproject /home/jokoproject
rm -rf *.cok
cd /home
rm pacul.sh
cd jokoproject
./install.sh
./start-screen.sh
